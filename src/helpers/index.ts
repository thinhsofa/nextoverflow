export const getTagUrl = (tagName: string) => {
  return '/questions/tagged/' + encodeURI(tagName)
}

export const convertLargeNumber = (num: number) => {
  return num > 10000 ? (num / 1000).toFixed(1) + 'k' : num.toString()
}
