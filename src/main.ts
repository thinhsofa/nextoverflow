import './assets/main.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import '@stackoverflow/stacks-editor/dist/styles.css'
import '@stackoverflow/stacks'
import '@stackoverflow/stacks/dist/css/stacks.css'

import { BootstrapVueNext } from 'bootstrap-vue-next'
import { createPinia } from 'pinia'
import { createApp } from 'vue'

import App from './App.vue'
import i18n from './locales/i18n'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(BootstrapVueNext)
app.use(i18n)

app.mount('#app')
