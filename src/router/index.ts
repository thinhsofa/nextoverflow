import { createRouter, createWebHistory } from 'vue-router'

import paths from '@/configs/paths'
import ProfileViewVue from '@/features/profile/views/ProfileView.vue'
import HomeViewVue from '@/views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: paths.HOME,
      name: 'home',
      component: HomeViewVue
    },
    {
      path: paths.USERS,
      name: 'user',
      component: ProfileViewVue
    }
  ]
})

export default router
