const tag = {
  'related-tags': 'Các thẻ liên quan',
  'view-tag': 'Xem thẻ',
  'more-related-tags': 'nhiều thẻ hơn',
  question: '{0} câu hỏi',
  questions: '{0} câu hỏi',
  watcher: '{0} lượt xem',
  watchers: '{0} lượt xem',
  'show-question-tag': "Xem câu hỏi thẻ '{0}'"
}
export default tag
