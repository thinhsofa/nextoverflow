const tag = {
  'related-tags': 'Related Tags',
  'view-tag': 'View tag',
  'more-related-tags': 'more related tags',
  question: '{0} question',
  questions: '{0} questions',
  watcher: '{0} watcher',
  watchers: '{0} watchers',
  'show-question-tag': "Show question tag '{0}'"
}
export default tag
