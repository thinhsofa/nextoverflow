import footer from './footer'
import profile from './profile'
import tag from './tag'

const en = { footer, profile, tag }

export default en
