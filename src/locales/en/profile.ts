const profile = {
  'member-for': 'Member for',
  years: 'years',
  month: 'month',
  'last-seen': 'Last seen',
  visited: 'Visited',
  days: 'days',
  consecutive: 'consecutive',
  Communities: 'Communities',
  Edit: 'Edit',
  Stats: 'Stats',
  reputation: 'reputation',
  reached: 'reached',
  answer: 'answer',
  question: 'question'
}
export default profile
