import type { Badge } from '../types'

export function getProfileBadges() {
  return [
    {
      type: 'silver',
      title: 'Japanese',
      archivedAt: 1672485081
    },
    {
      type: 'gold',
      title: 'Maltese',
      archivedAt: 1685483367
    },
    {
      type: 'gold',
      title: 'Somali',
      archivedAt: 1667421202
    },
    {
      type: 'gold',
      title: 'Indonesian',
      archivedAt: 1645023292
    },
    {
      type: 'silver',
      title: 'Punjabi',
      archivedAt: 1655728365
    },
    {
      type: 'gold',
      title: 'Japanese',
      archivedAt: 1684691426
    },
    {
      type: 'silver',
      title: 'Bislama',
      archivedAt: 1661541385
    },
    {
      type: 'silver',
      title: 'Hindi',
      archivedAt: 1666674525
    },
    {
      type: 'gold',
      title: 'Czech',
      archivedAt: 1656041752
    },
    {
      type: 'bronze',
      title: 'Bislama',
      archivedAt: 1668629724
    },
    {
      type: 'silver',
      title: 'Luxembourgish',
      archivedAt: 1669294417
    },
    {
      type: 'bronze',
      title: 'Tetum',
      archivedAt: 1641920450
    },
    {
      type: 'silver',
      title: 'Dutch',
      archivedAt: 1675943355
    },
    {
      type: 'silver',
      title: 'Swedish',
      archivedAt: 1667414829
    },
    {
      type: 'bronze',
      title: 'Assamese',
      archivedAt: 1663163297
    },
    {
      type: 'bronze',
      title: 'Hiri Motu',
      archivedAt: 1651608563
    },
    {
      type: 'bronze',
      title: 'Dhivehi',
      archivedAt: 1642850085
    },
    {
      type: 'gold',
      title: 'Sotho',
      archivedAt: 1659638162
    },
    {
      type: 'silver',
      title: 'Dutch',
      archivedAt: 1684141268
    },
    {
      type: 'bronze',
      title: 'Icelandic',
      archivedAt: 1682484372
    },
    {
      type: 'silver',
      title: 'Papiamento',
      archivedAt: 1681655223
    },
    {
      type: 'bronze',
      title: 'Gagauz',
      archivedAt: 1681569326
    },
    {
      type: 'gold',
      title: 'Kannada',
      archivedAt: 1677729195
    },
    {
      type: 'gold',
      title: 'Pashto',
      archivedAt: 1674585693
    },
    {
      type: 'silver',
      title: 'Sotho',
      archivedAt: 1666637475
    },
    {
      type: 'bronze',
      title: 'Zulu',
      archivedAt: 1668774034
    },
    {
      type: 'bronze',
      title: 'Catalan',
      archivedAt: 1657237116
    },
    {
      type: 'bronze',
      title: 'Macedonian',
      archivedAt: 1683051596
    },
    {
      type: 'gold',
      title: 'Polish',
      archivedAt: 1685407647
    },
    {
      type: 'silver',
      title: 'Malayalam',
      archivedAt: 1657346233
    },
    {
      type: 'bronze',
      title: 'Kazakh',
      archivedAt: 1658520261
    },
    {
      type: 'silver',
      title: 'Georgian',
      archivedAt: 1677696257
    },
    {
      type: 'silver',
      title: 'Hungarian',
      archivedAt: 1678679830
    },
    {
      type: 'bronze',
      title: 'Japanese',
      archivedAt: 1669768370
    },
    {
      type: 'gold',
      title: 'New Zealand Sign Language',
      archivedAt: 1657900646
    },
    {
      type: 'bronze',
      title: 'Moldovan',
      archivedAt: 1655765237
    },
    {
      type: 'bronze',
      title: 'Lithuanian',
      archivedAt: 1643220929
    },
    {
      type: 'silver',
      title: 'Estonian',
      archivedAt: 1652500537
    },
    {
      type: 'gold',
      title: 'Danish',
      archivedAt: 1642993486
    },
    {
      type: 'silver',
      title: 'Tsonga',
      archivedAt: 1654334180
    },
    {
      type: 'gold',
      title: 'Assamese',
      archivedAt: 1660049927
    },
    {
      type: 'gold',
      title: 'Italian',
      archivedAt: 1669901953
    },
    {
      type: 'silver',
      title: 'Persian',
      archivedAt: 1663330477
    },
    {
      type: 'silver',
      title: 'Maltese',
      archivedAt: 1662692746
    },
    {
      type: 'gold',
      title: 'Armenian',
      archivedAt: 1647816390
    },
    {
      type: 'bronze',
      title: 'Khmer',
      archivedAt: 1644275561
    },
    {
      type: 'gold',
      title: 'Georgian',
      archivedAt: 1662682871
    },
    {
      type: 'silver',
      title: 'Marathi',
      archivedAt: 1669148041
    },
    {
      type: 'silver',
      title: 'Ndebele',
      archivedAt: 1646276709
    },
    {
      type: 'silver',
      title: 'Hungarian',
      archivedAt: 1656023607
    },
    {
      type: 'gold',
      title: 'Irish Gaelic',
      archivedAt: 1657750281
    },
    {
      type: 'gold',
      title: 'German',
      archivedAt: 1669804409
    },
    {
      type: 'bronze',
      title: 'Portuguese',
      archivedAt: 1670843627
    },
    {
      type: 'gold',
      title: 'Tamil',
      archivedAt: 1670536838
    },
    {
      type: 'silver',
      title: 'Tamil',
      archivedAt: 1681648300
    },
    {
      type: 'silver',
      title: 'Latvian',
      archivedAt: 1654750208
    },
    {
      type: 'silver',
      title: 'Portuguese',
      archivedAt: 1668144636
    },
    {
      type: 'bronze',
      title: 'Finnish',
      archivedAt: 1670914590
    },
    {
      type: 'silver',
      title: 'Arabic',
      archivedAt: 1681069820
    },
    {
      type: 'silver',
      title: 'Luxembourgish',
      archivedAt: 1651744138
    },
    {
      type: 'gold',
      title: 'Thai',
      archivedAt: 1652753664
    },
    {
      type: 'bronze',
      title: 'Latvian',
      archivedAt: 1674195957
    },
    {
      type: 'bronze',
      title: 'Swedish',
      archivedAt: 1664372590
    },
    {
      type: 'silver',
      title: 'Romanian',
      archivedAt: 1672326455
    },
    {
      type: 'silver',
      title: 'Somali',
      archivedAt: 1658365384
    },
    {
      type: 'bronze',
      title: 'Tamil',
      archivedAt: 1678781498
    },
    {
      type: 'bronze',
      title: 'Arabic',
      archivedAt: 1684258409
    },
    {
      type: 'silver',
      title: 'Nepali',
      archivedAt: 1663444316
    },
    {
      type: 'gold',
      title: 'Moldovan',
      archivedAt: 1680537230
    },
    {
      type: 'bronze',
      title: 'Hebrew',
      archivedAt: 1643595526
    },
    {
      type: 'bronze',
      title: 'Malayalam',
      archivedAt: 1660909386
    },
    {
      type: 'bronze',
      title: 'Assamese',
      archivedAt: 1643307573
    },
    {
      type: 'silver',
      title: 'Aymara',
      archivedAt: 1661157659
    },
    {
      type: 'bronze',
      title: 'Armenian',
      archivedAt: 1670851572
    },
    {
      type: 'bronze',
      title: 'Kannada',
      archivedAt: 1649612137
    },
    {
      type: 'bronze',
      title: 'Zulu',
      archivedAt: 1669156148
    },
    {
      type: 'bronze',
      title: 'Albanian',
      archivedAt: 1648642843
    },
    {
      type: 'gold',
      title: 'West Frisian',
      archivedAt: 1662150174
    },
    {
      type: 'silver',
      title: 'Tetum',
      archivedAt: 1672753201
    },
    {
      type: 'silver',
      title: 'Kazakh',
      archivedAt: 1661474201
    },
    {
      type: 'silver',
      title: 'Somali',
      archivedAt: 1647182973
    },
    {
      type: 'silver',
      title: 'Kannada',
      archivedAt: 1664430797
    },
    {
      type: 'gold',
      title: 'Kyrgyz',
      archivedAt: 1683686566
    },
    {
      type: 'silver',
      title: 'Marathi',
      archivedAt: 1660835519
    },
    {
      type: 'silver',
      title: 'Malagasy',
      archivedAt: 1676932083
    },
    {
      type: 'bronze',
      title: 'Romanian',
      archivedAt: 1651612095
    },
    {
      type: 'silver',
      title: 'Assamese',
      archivedAt: 1655123047
    },
    {
      type: 'bronze',
      title: 'Dzongkha',
      archivedAt: 1665147879
    },
    {
      type: 'silver',
      title: 'Mongolian',
      archivedAt: 1651524432
    },
    {
      type: 'silver',
      title: 'Croatian',
      archivedAt: 1682638483
    },
    {
      type: 'silver',
      title: 'French',
      archivedAt: 1648114116
    },
    {
      type: 'bronze',
      title: 'Macedonian',
      archivedAt: 1659386492
    },
    {
      type: 'gold',
      title: 'Portuguese',
      archivedAt: 1652533025
    },
    {
      type: 'silver',
      title: 'Bengali',
      archivedAt: 1677238720
    },
    {
      type: 'silver',
      title: 'Catalan',
      archivedAt: 1680275980
    },
    {
      type: 'silver',
      title: 'Arabic',
      archivedAt: 1658334693
    },
    {
      type: 'silver',
      title: 'Korean',
      archivedAt: 1669458451
    },
    {
      type: 'gold',
      title: 'Yiddish',
      archivedAt: 1644240939
    },
    {
      type: 'bronze',
      title: 'Hiri Motu',
      archivedAt: 1651878745
    },
    {
      type: 'silver',
      title: 'Tajik',
      archivedAt: 1665403319
    },
    {
      type: 'gold',
      title: 'Maltese',
      archivedAt: 1685344037
    },
    {
      type: 'silver',
      title: 'Yiddish',
      archivedAt: 1654354978
    },
    {
      type: 'bronze',
      title: 'Hebrew',
      archivedAt: 1676257694
    },
    {
      type: 'gold',
      title: 'Azeri',
      archivedAt: 1675121882
    },
    {
      type: 'silver',
      title: 'Estonian',
      archivedAt: 1668631540
    },
    {
      type: 'bronze',
      title: 'Zulu',
      archivedAt: 1650178835
    },
    {
      type: 'gold',
      title: 'Gagauz',
      archivedAt: 1665782703
    },
    {
      type: 'silver',
      title: 'Kyrgyz',
      archivedAt: 1672112234
    },
    {
      type: 'bronze',
      title: 'Gujarati',
      archivedAt: 1648168305
    }
  ] satisfies Badge[]
}
