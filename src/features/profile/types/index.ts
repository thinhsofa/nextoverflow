type EpochTime = number

type Badge = {
  type: 'gold' | 'silver' | 'bronze'
  title: string
  archivedAt: EpochTime
}

export type { Badge }
