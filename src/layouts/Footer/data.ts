export type TLink = {
  id: number
  label: string
  url: string
}
export type TLinkNav = {
  id: number
  title: TLink
  details: TLink[]
}

export const LOGO_LINK = '/'

export const NAV_LINK: TLinkNav[] = [
  {
    id: 0,
    title: { id: 0, label: 'next-overflow', url: '/' },
    details: [
      { id: 0, label: 'questions', url: '/' },
      { id: 1, label: 'help', url: '/' }
    ]
  },
  {
    id: 1,
    title: { id: 0, label: 'products', url: '/' },
    details: [
      { id: 0, label: 'teams', url: '/' },
      { id: 1, label: 'advertising', url: '/' },
      { id: 2, label: 'collectives', url: '/' },
      { id: 3, label: 'talent', url: '/' }
    ]
  },
  {
    id: 3,
    title: { id: 0, label: 'company', url: '/' },
    details: [
      { id: 0, label: 'about', url: '/' },
      { id: 1, label: 'press', url: '/' },
      { id: 2, label: 'work-here', url: '/' },
      { id: 3, label: 'legal', url: '/' },
      { id: 4, label: 'privacy-policy', url: '/' },
      { id: 5, label: 'terms-of-service', url: '/' },
      { id: 6, label: 'contact-us', url: '/' },
      { id: 7, label: 'cookie-settings', url: '/' },
      { id: 8, label: 'cookie-policy', url: '/' }
    ]
  },
  {
    id: 4,
    title: {
      id: 0,
      label: 'stack-exchange',
      url: '/'
    },
    details: [
      { id: 0, label: 'technology', url: '/' },
      {
        id: 1,
        label: 'culture-recreation',
        url: '/'
      },
      { id: 2, label: 'life-arts', url: '/' },
      { id: 3, label: 'science', url: '/' },
      { id: 4, label: 'professional', url: '/' },
      { id: 5, label: 'business', url: '/' },
      { id: 6, label: 'API', url: '/' },
      { id: 7, label: 'data', url: '/' }
    ]
  }
]

export const SOCIAL_LINK: TLink[] = [
  { id: 0, label: 'blog', url: 'https://stackoverflow.blog/?blb=1' },
  {
    id: 1,
    label: 'facebook',
    url: 'https://www.facebook.com/officialstackoverflow/'
  },
  { id: 2, label: 'twitter', url: 'https://twitter.com/stackoverflow' },
  {
    id: 3,
    label: 'linkedIn',
    url: 'https://www.linkedin.com/company/stack-overflow'
  },
  {
    id: 4,
    label: 'instagram',
    url: 'https://www.instagram.com/thestackoverflow/'
  }
]

export const LICENSING = 'https://stackoverflow.com/help/licensing'
