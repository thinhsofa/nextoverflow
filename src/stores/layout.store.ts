import { useToggle } from '@vueuse/core'
import { defineStore } from 'pinia'
import { computed } from 'vue'

export const useLayoutStore = defineStore('layout', () => {
  const [mobile, toggleIsMobile] = useToggle(false)
  const isMobile = computed(() => mobile.value)

  return {
    isMobile,
    toggleIsMobile
  }
})
