const paths = {
  HOME: '/',
  QUESTIONS: '/questions',
  TAGS: '/tags',
  USERS: '/users',
  COMPANIES: '/companies',
  COLLECTIONS: '/collections',
  TEAMS: '/teams',
  PROFILE: '/profile'
} as const

export default paths
